import json
import csv
from airflow import DAG

from airflow.operators.python import PythonOperator
from airflow.operators.dummy import DummyOperator
from airflow.utils.dates import days_ago

from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.models.variable import Variable
default_args = {
    'owner':'segolf',
}
root = Variable.get('file_search_path')

with DAG(
    'testing_postgres',
    default_args=default_args,
    description='checking connection to postgres',
    schedule_interval=None,
    start_date=days_ago(2),
    tags=['seth']
) as dag:


    def pull_orders():
        sql=root + 'sql/testing_copy_to_csv.sql'
        pg_hook = PostgresHook(postgres_conn_id='postgres_default')
        connection = pg_hook.get_conn()
        cursor = connection.cursor()
        cursor.execute(open(sql, "r").read())
        data = cursor.fetchall()
        with open(root + 'temp_storage/rl_orders.csv', 'w') as f:
            writer = csv.writer(f, delimiter=',')
            for row in data:
                writer.writerow(row)



    extract_task = PythonOperator(
        task_id='extract_task_id',
        python_callable=pull_orders
    )

    start_task = DummyOperator(
        task_id= 'start_task_id'
    )


    start_task >> extract_task