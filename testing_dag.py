from airflow import DAG
from airflow.models.variable import Variable
from datetime import timedelta
from airflow.utils.dates import days_ago
import json
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.operators.python import PythonOperator
from psycopg2 import extras

default_args = {
    'owner':'segolf',
    'retries': 1,
    'retry_delay': timedelta(seconds=1)
}

root = Variable.get('temp_extract_path')

with DAG(
    'testing_dag',
    default_args=default_args,
    description='remaking internal metrics downloads',
    schedule_interval=None,
    start_date= days_ago(2),
    tags=['seth']
) as dag:

    def cs_proactive_load(connection_id):
        with open(root + '/extracts/hubspot_companies_extract.json', 'r') as f:
            data = json.load(f)

        sql = 'insert into internal_metrics.hubspot_companies_raw ' \
                  '(id,properties,createdAt,updatedAt,archived,associations) values %s on conflict (id) do update ' \
                    'set properties = excluded.properties, ' \
                'createdAt = excluded.createdAt, ' \
                'updatedAt = excluded.updatedAt, ' \
                'archived = excluded.archived, ' \
                'associations = excluded.associations' \
              'where updateAt != excluded.updatedAt;'

        pg_hook = PostgresHook(postgres_conn_id=connection_id)
        connection = pg_hook.get_conn()
        cursor = connection.cursor()
        extras.execute_values(cursor, sql, data,
                              template="(%(id)s,"
                                       " Json%(properties)s,"
                                       " %(createdAt)s,"
                                       " %(updatedAt)s,"
                                       " %(archived)s,"
                                       " Json%(associations)s)",
                              page_size=10000,
                              fetch=False)
        connection.commit()

    cs_proactive_load = PythonOperator(
        task_id='testing',
        python_callable=cs_proactive_load,
        op_kwargs={'connection_id': 'marketing_conn_id'},
        dag=dag
    )

    cs_proactive_load