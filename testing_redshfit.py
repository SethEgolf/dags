import csv

from airflow import DAG

from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.utils.dates import days_ago

from airflow.models.variable import Variable


default_args = {
    'owner':'segolf',
}
root = Variable.get('file_search_path')

with DAG(
    'testing_redshift',
    default_args=default_args,
    description='making sure I can connect to Redshift',
    schedule_interval=None,
    start_date=days_ago(2),
    tags=['seth']
) as dag:

    b_script = 'networksetup -connectpppoeservice "ReturnLogic"'

    def redshift_connection():
        sql= root + 'sql/testing_redshift.sql'
        rs_hook = PostgresHook(postgres_conn_id='redshift_default')
        connection = rs_hook.get_conn()
        cursor = connection.cursor()
        cursor.execute(open(sql, "r").read())
        data = cursor.fetchall()
        with open(root + 'temp_storage/rs_rl_orders.csv', 'w') as f:
            writer = csv.writer(f, delimiter=',')
            for row in data:
                writer.writerow(row)



    start_task = DummyOperator(
        task_id= 'start_task',
        dag=dag
    )

    connect_vpn_task = BashOperator(
        task_id='connect_VPN',
        bash_command=b_script,
        dag=dag
    )

    pull_data_task = PythonOperator(
        task_id= 'pull_data',
        python_callable= redshift_connection,
        dag=dag
    )

    start_task >> connect_vpn_task >> pull_data_task