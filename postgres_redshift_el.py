import csv
from datetime import datetime
from dateutil import parser
from dateutil.relativedelta import relativedelta
from airflow import DAG

from airflow.operators.python import PythonOperator
from airflow.operators.dummy import DummyOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.operators.bash import BashOperator

from airflow.utils.dates import days_ago
from airflow.models.variable import Variable

default_args = {
    'owner':'segolf'
}

root = Variable.get('file_search_path')

with DAG(
    'postgres_redshift_el',
    default_args=default_args,
    description='creating basic EL job between postgres and redshfit',
    schedule_interval=None,
    start_date= days_ago(2),
    tags=['seth']
) as dag:

    b_script = 'networksetup -connectpppoeservice "ReturnLogic"'

    def check_max_create_date(schema,connection_id):
        sql='select max(create_date)::text from ' + schema + 'rl_order_items'
        pg_hook = PostgresHook(postgres_conn_id=connection_id)
        connection = pg_hook.get_conn()
        cursor = connection.cursor()
        cursor.execute(sql)
        data = cursor.fetchone()
        return str(data[0])



    def extract(**kwargs):
        ti =kwargs['ti']
        date = ti.xcom_pull(task_ids='check_redhsift_max_date', key='return_value')
        sql = "copy (select * from rlap.rl_order_items where update_date between '" + str(date) + "' and '" + str(date) + "'::timestamp + interval '1 day') to stdout csv header null 'NULL'  escape '\\'  "
        pg_hook = PostgresHook(postgres_conn_id='postgres_default')
        pg_hook.copy_expert(sql, root + 'temp_storage/rl_orders_copy.csv')

    def format():
        with open(root + 'temp_storage/rl_orders_copy.csv' , 'r') as read_file:
            reader = csv.reader(read_file)
            columns = "("+str(next(reader)).replace("'",'')[1:-1]+")"
            values = ''
            for row in reader:
                values = values + "(" + str(row)[1:-1] + "),"
            values = values[0:-1].replace("'NULL'",'NULL')
            with open(root+ 'sql/multirow.sql', 'w') as wrtie_file:
                wrtie_file.write('insert into airflow_schema.temp_rl_order_items ' + columns + ' VALUES ' + values)

    def load():
        sql = root + 'sql/multirow.sql'
        pg_hook = PostgresHook(postgres_conn_id='redshift_default')
        connection = pg_hook.get_conn()
        cursor = connection.cursor()
        cursor.execute(open(sql, "r").read())

    start_task = DummyOperator(
        task_id='start_task',
        dag=dag
    )

    connect_vpn_task = BashOperator(
        task_id='connect_VPN',
        bash_command=b_script,
        dag=dag
    )

    check_max_date_postgres_task = PythonOperator(
        task_id='check_postgres_max_date',
        python_callable=check_max_create_date,
        op_kwargs={'schema':'rlap.','connection_id':'postgres_default'},
        dag=dag
    )

    check_max_date_redshift_task = PythonOperator(
        task_id='check_redhsift_max_date',
        python_callable=check_max_create_date,
        op_kwargs={'schema': 'airflow_schema.', 'connection_id': 'redshift_default'},
        dag=dag
    )


    extract_date_range = PythonOperator(
        task_id='extract_date_range',
        python_callable= extract,
        dag=dag
    )

    format_load_sql = PythonOperator(
        task_id='format_load_sql',
        python_callable=format,
        dag=dag
    )

    load_stage = PythonOperator(
        task_id='load_stage_table',
        python_callable=load,
        dag=dag
    )

    merge_tables = DummyOperator(
        task_id='merge_tables',
        dag=dag
    )

    connect_vpn_task >> check_max_date_redshift_task
    start_task >> [check_max_date_postgres_task, connect_vpn_task]
    [check_max_date_postgres_task, check_max_date_redshift_task] >> extract_date_range
    extract_date_range >> format_load_sql >>load_stage >> merge_tables