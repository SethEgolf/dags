from airflow import DAG

from airflow.operators.python import PythonOperator
from airflow.operators.dummy import DummyOperator
from airflow.utils.dates import days_ago

from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.models.variable import Variable


default_args = {
    'owner':'segolf',
}
root = Variable.get('file_search_path')

with DAG(
    'testing_redshift_copy_expert',
    default_args=default_args,
    description='is copy expert a viable opotion',
    schedule_interval=None,
    start_date=days_ago(2),
    tags=['seth']
) as dag:


    def pull_orders():
        sql=root + 'sql/redshift_copy_expert.sql'
        pg_hook = PostgresHook(postgres_conn_id='redshift_default')
        pg_hook.copy_expert((open(sql,'r').read()),root+'temp_storage/redshift_orders_copy.csv')

    extract_task = PythonOperator(
        task_id='copy_expert_task',
        python_callable=pull_orders
    )

    start_task = DummyOperator(
        task_id= 'start_task'
    )


    start_task >> extract_task