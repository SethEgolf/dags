from airflow import DAG

from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.utils.dates import days_ago


from airflow.models.variable import Variable


default_args ={
    'owner':'segolf',
}

root = Variable.get('file_search_path')

with DAG(
    'Basic_transform_scheduler',
    default_args=default_args,
    description='uses stitch and defined sps to update tables',
    start_date=days_ago(2),
    tags=['seth']
) as dag:


    b_script = 'networksetup -connectpppoeservice "ReturnLogic"'



    start_task = DummyOperator(
        task_id='start_task',
        dag=dag
    )

    connect_vpn_task = BashOperator(
        task_id='connect_VPN',
        bash_command=b_script,
        dag=dag
    )


    update_total_rollup_team_id_date_task = PostgresOperator(
        task_id='update_total_rollup_team_id_date',
        postgres_conn_id='redshift_default',
        sql= 'call airflow_schema.sp_etl_total_rollup_team_id_date();',
        dag=dag
    )

    update_customer_rollup_task = PostgresOperator(
        task_id='update_customer_rollup_task',
        postgres_conn_id='redshift_default',
        sql='call airflow_schema.sp_etl_customer_rollup();',
        dag=dag
    )

    update_return_reason_rollup_task = PostgresOperator(
        task_id='update_return_reason_rollup',
        postgres_conn_id='redshift_default',
        sql='call airflow_schema.sp_etl_return_reason_rollup();',
        dag=dag
    )

    update_disposition_rollup_task = PostgresOperator(
        task_id='update_disposition_rollup',
        postgres_conn_id='redshift_default',
        sql='call airflow_schema.sp_etl_dispositions_rollup();',
        dag=dag
    )

    update_return_type_task = PostgresOperator(
        task_id='update_return_type',
        postgres_conn_id='redshift_default',
        sql='call airflow_schema.sp_etl_return_type();',
        dag=dag
    )

    update_product_metrics_task = PostgresOperator(
        task_id='update_product_metrics',
        postgres_conn_id='redshift_default',
        sql='call airflow_schema.sp_etl_product_metrics();',
        dag=dag
    )



    start_task >> connect_vpn_task >> [update_total_rollup_team_id_date_task, update_customer_rollup_task, update_return_reason_rollup_task, update_disposition_rollup_task, update_return_type_task, update_product_metrics_task]