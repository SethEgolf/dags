
from airflow import DAG
from datetime import timedelta
from airflow.operators.python import PythonOperator
from airflow.operators.dummy import DummyOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from psycopg2 import extras

from airflow.utils.dates import days_ago
from airflow.models.variable import Variable
import requests
import json
import jsonpickle
import re

import recurly

default_args = {
    'owner':'segolf',
    'retries': 3,
    'retry_delay': timedelta(seconds=1)
}

root = Variable.get('temp_extract_path')
h_key = Variable.get('Hubspot_key')
i_token = Variable.get('Inter_token')
r_key = Variable.get('Recurly_key')

with DAG(
    'Update_data_lake',
    default_args=default_args,
    description='remaking internal metrics downloads',
    schedule_interval=None,
    start_date= days_ago(2),
    tags=['seth']
) as dag:


    def Hubspot_companies_extract():
        url = "https://api.hubapi.com/crm/v3/properties/companies"
        querystring = {"archived": "false", "hapikey": h_key}
        headers = {'accept': 'application/json'}

        response = requests.get(url, headers=headers, params=querystring)
        response_dict = json.loads(response.text)
        properties_string = ''
        properties_length = len(response_dict['results'])
        json_list = []

        print('adding properties to url string')
        for x in range(properties_length - 2):
            properties_string = properties_string + response_dict['results'][x]['name'] + ','
        properties_string = properties_string + response_dict['results'][properties_length - 1]['name']


        # set up for pulling comapanies
        url = "https://api.hubapi.com/crm/v3/objects/companies?acrchived=false&limit=100&hapikey="+h_key+"&properties=" + properties_string + "&associations=companies,contacts,deals"
        print('calling get request')
        response_dict = requests.get(url)
        r = json.loads(response_dict.text)
        paging = r.get('paging', 'false')
        json_list.extend(r.get('results'))
        print(len(json_list))
        while paging != 'false':
            next_page = paging.get('next', 'false')
            after = next_page.get('after', 'false')
            new_url = url + '&after=' + after
            response_dict = requests.get(new_url)
            r = json.loads(response_dict.text)
            paging = r.get('paging', 'false')
            json_list.extend(r['results'])

        # returning  list
        with open(root+'/extracts/hubspot_companies_extract.json', 'w') as file:
            json.dump(json_list, file)

    def Hubspot_contacts_extract():
        url = "https://api.hubapi.com/crm/v3/properties/contacts"
        querystring = {"archived": "false", "hapikey": h_key}
        headers = {'accept': 'application/json'}

        response = requests.get(url, headers=headers, params=querystring)
        response_dict = json.loads(response.text)
        properties_string = ''
        properties_length = len(response_dict['results'])
        json_list = []

        print('adding properties to url string')
        for x in range(properties_length - 2):
            properties_string = properties_string + response_dict['results'][x]['name'] + ','
        properties_string = properties_string + response_dict['results'][properties_length - 1]['name']

        # set up for pulling comapanies
        url = "https://api.hubapi.com/crm/v3/objects/contacts?acrchived=false&limit=100&hapikey=" + h_key + "&properties=" + properties_string + "&associations=companies,contacts,deals"
        print('calling get request')
        response_dict = requests.get(url)
        r = json.loads(response_dict.text)
        paging = r.get('paging', 'false')
        json_list.extend(r.get('results'))
        print(len(json_list))
        while paging != 'false':
            next_page = paging.get('next', 'false')
            after = next_page.get('after', 'false')
            new_url = url + '&after=' + after
            response_dict = requests.get(new_url)
            r = json.loads(response_dict.text)
            paging = r.get('paging', 'false')
            json_list.extend(r['results'])

        # returning  list
        with open(root + '/extracts/hubspot_contacts_extract.json', 'w') as file:
            json.dump(json_list, file)

    def Hubspot_deals_extract():
        url = "https://api.hubapi.com/crm/v3/properties/deals"
        querystring = {"archived": "false", "hapikey": h_key}
        headers = {'accept': 'application/json'}

        response = requests.get(url, headers=headers, params=querystring)
        response_dict = json.loads(response.text)
        properties_string = ''
        properties_length = len(response_dict['results'])
        json_list = []

        print('adding properties to url string')
        for x in range(properties_length - 2):
            properties_string = properties_string + response_dict['results'][x]['name'] + ','
        properties_string = properties_string + response_dict['results'][properties_length - 1]['name']

        # set up for pulling comapanies
        url = "https://api.hubapi.com/crm/v3/objects/deals?acrchived=false&limit=100&hapikey=" + h_key + "&properties=" + properties_string + "&associations=companies,contacts,deals"
        print('calling get request')
        response_dict = requests.get(url)
        r = json.loads(response_dict.text)
        paging = r.get('paging', 'false')
        json_list.extend(r.get('results'))
        print(len(json_list))
        while paging != 'false':
            next_page = paging.get('next', 'false')
            after = next_page.get('after', 'false')
            new_url = url + '&after=' + after
            response_dict = requests.get(new_url)
            r = json.loads(response_dict.text)
            paging = r.get('paging', 'false')
            json_list.extend(r['results'])

        # returning  list
        with open(root + '/extracts/hubspot_deals_extract.json', 'w') as file:
            json.dump(json_list, file)


    def Hubspot_deals_pipeline_extract():
        url = 'https://api.hubapi.com/crm/v3/pipelines/deals?hapikey=' + h_key

        r = requests.get(url)
        r = json.loads(r.text)
        with open(root + '/extracts/hubspot_deals_pipeline_extract.json', 'w') as file:
            json.dump(r, file)


    def Hubspot_engagements_extract():
        url = 'https://api.hubapi.com/engagements/v1/engagements/paged?hapikey=' + h_key + '&limit=250'
        r = requests.get(url)
        r = json.loads(r.text)
        has_more = r.get('hasMore')
        response_dict = r.get('results')
        while has_more == True:
            next = r.get('offset')
            new_url = url + '&offset=' + str(next)
            r = requests.get(new_url)
            r = json.loads(r.text)
            response_dict.extend(r['results'])
            has_more = r.get('hasMore')

        with open(root + '/extracts/hubspot_engagements_extract.json', 'w') as outfile:
            json.dump(response_dict, outfile)

    def intercom_contacts_extract():
        url = 'https://api.intercom.io/contacts'
        params = {'per_page': 150}
        headers = {'Authorization': 'Bearer ' + i_token, 'Accept': 'application/json'}

        r = requests.get(url, headers=headers, params=params)
        r = json.loads(r.text)
        max_page = r['pages']['total_pages']
        current_page = r['pages']['page']
        response_dict = r['data']
        while current_page != max_page:
            starting_after_value = r['pages']['next']['starting_after']
            params = {'per_page': 150, 'starting_after': starting_after_value}
            r = requests.get(url, params=params, headers=headers)
            r = json.loads(r.text)
            current_page = r['pages']['page']
            max_page = r['pages']['total_pages']
            response_dict.extend(r['data'])

        with open(root + '/extracts/intercom_contacts_extract.json', 'w') as outfile:
            json.dump(response_dict, outfile)

    def intercom_companies_extract():
        url = 'https://api.intercom.io/companies/scroll'
        params = {}
        headers = {'Authorization': 'Bearer ' + i_token, 'Accept': 'application/json'}

        r = requests.get(url, params=params, headers=headers)
        r = json.loads(r.text)
        check = r['data']
        scroll_param = r['scroll_param']
        params = {'scroll_param': scroll_param}
        response_dict = r['data']
        while check != []:
            r = requests.get(url, params=params, headers=headers)
            r = json.loads(r.text)
            check = r['data']
            response_dict.extend(check)

        with open(root + '/extracts/intercom_companies_extract.json', 'w') as outfile:
            json.dump(response_dict, outfile)

    def intercom_conversations_extract():
        url = 'https://api.intercom.io/conversations'
        params = {'per_page': 20}
        headers = {'Authorization': 'Bearer ' + i_token, 'Accept': 'application/json'}

        r = requests.get(url, params=params, headers=headers)
        r = json.loads(r.text)
        max_page = r['pages']['total_pages']
        current_page = r['pages']['page']
        response_dict = r['conversations']
        while current_page != max_page:
            url = r['pages']['next']
            r = requests.get(url, headers=headers)
            r = json.loads(r.text)
            current_page = r['pages']['page']
            max_page = r['pages']['total_pages']
            response_dict.extend(r['conversations'])

        with open(root + '/extracts/intercom_conversations_extract.json', 'w') as outfile:
            json.dump(response_dict, outfile)

    def recurly_accounts_extract():
        response_dict = []
        client = recurly.Client(r_key)
        items = client.list_accounts().items()
        for item in items:
            response_dict.append(item.__dict__)

        with open(root + '/extracts/recurly_accounts_extract.json', 'w') as outfile:
            outfile.write(jsonpickle.encode(response_dict))

    def recurly_subscriptions_extract():
        response_dict = []
        client = recurly.Client(r_key)
        items = client.list_subscriptions().items()
        for item in items:
            response_dict.append(item.__dict__)

        with open(root + '/extracts/recurly_subscriptions_extract.json', 'w') as outfile:
            outfile.write(jsonpickle.encode(response_dict))

    def recurly_invoices_extract():
        response_dict = []
        client = recurly.Client(r_key)
        items = client.list_invoices().items()
        for item in items:
            response_dict.append(item.__dict__)

        with open(root + '/extracts/recurly_invoices_extract.json', 'w') as outfile:
            outfile.write(jsonpickle.encode(response_dict))


    start_extracts = DummyOperator(
        task_id='start_extracts',
        dag=dag
    )

    Hubspot_companies_extract = PythonOperator(
        task_id='Hubspot_companies_extract',
        python_callable=Hubspot_companies_extract,
        dag=dag
    )

    Hubspot_contacts_extact = PythonOperator(
        task_id='Hubspot_contacts_extract',
        python_callable=Hubspot_contacts_extract,
        dag=dag
    )

    Hubspot_deals_extact = PythonOperator(
        task_id='Hubspot_deals_extract',
        python_callable=Hubspot_deals_extract,
        dag=dag
    )

    Hubspot_deals_pipeline_extract = PythonOperator(
        task_id='Hubspot_deals_pipeline_extract',
        python_callable=Hubspot_deals_pipeline_extract,
        dag=dag
    )

    Hubspot_engagements_extact = PythonOperator(
        task_id='Hubspot_engagements_extract',
        python_callable=Hubspot_engagements_extract,
        dag=dag
    )

    intercom_companies_extract  = PythonOperator(
        task_id='intercom_companies_extract',
        python_callable=intercom_companies_extract,
        dag=dag
    )

    intercom_contacts_extract = PythonOperator(
        task_id='intercom_contacts_extract',
        python_callable=intercom_contacts_extract,
        dag=dag
    )

    intercom_conversations_extract = PythonOperator(
        task_id='intercom_conversations_extract',
        python_callable=intercom_conversations_extract,
        dag=dag
    )

    recurly_accounts_extract = PythonOperator(
        task_id='recurly_accounts_extract',
        python_callable=recurly_accounts_extract,
        dag=dag
    )

    recurly_subscriptions_extract = PythonOperator(
        task_id='recurly_subscriptions_extract',
        python_callable=recurly_subscriptions_extract,
        dag=dag
    )

    recurly_invoices_extract = PythonOperator(
        task_id='recurly_invoices_extract',
        python_callable=recurly_invoices_extract,
        dag=dag
    )

    start_transforms = DummyOperator(
        task_id='start_transforms',
        dag=dag
    )

    # Hubspot extracts
    start_extracts >> [Hubspot_companies_extract, Hubspot_contacts_extact, Hubspot_deals_extact, Hubspot_deals_pipeline_extract, Hubspot_engagements_extact] >> start_transforms

    # Intercom extracts
    start_extracts >> [intercom_contacts_extract, intercom_companies_extract, intercom_conversations_extract] >> start_transforms

    # Reculry extracts
    start_extracts >> [recurly_accounts_extract, recurly_subscriptions_extract, recurly_invoices_extract] >> start_transforms

    def cs_proactive_transform():
        with open(root + '/extracts/hubspot_engagements_extract.json', 'r') as infile:
            data = json.loads(infile.read())

        response_dict = []
        current_dict = {}
        for item in data:
            if re.search('EMAIL', item['engagement']['type']):
                current_dict['id'] = item['engagement']['id']
                current_dict['type'] = item['engagement']['type']
                current_dict['timestamp'] = item['engagement']['timestamp']
                current_dict['subject'] = item['metadata'].get('subject')
                current_dict['email'] = item['metadata'].get('from', {}).get('email', 'email not found')
                current_dict['response'] = str(current_dict['subject']).lower().startswith('re: ')
                response_dict.append(current_dict.copy())

        with open(root + '/Transforms/cs_proactive_transform.json', 'w') as outfile:
            json.dump(response_dict, outfile)

    cs_proactive_transform = PythonOperator(
        task_id='cs_proactive_transform',
        python_callable=cs_proactive_transform,
        dag=dag
    )

    start_load = DummyOperator(
        task_id='start_load',
        dag=dag
    )
    start_transforms >> cs_proactive_transform >> start_load


    def cs_proactive_load(connection_id):
        with open(root + '/Transforms/cs_proactive_transform.json', 'r') as f:
            data = json.load(f)

        sql = 'insert into internal_metrics.cs_proactivity ' \
              '(id,type,timestamp,subject,email,response) values %s on conflict (id) do update ' \
              'set type = excluded.type, ' \
              'timestamp = excluded.timestamp, ' \
              'subject = excluded.subject, ' \
              'email = excluded.email, ' \
              'response = excluded.response;'

        pg_hook = PostgresHook(postgres_conn_id=connection_id)
        connection = pg_hook.get_conn()
        cursor = connection.cursor()
        extras.execute_values(cursor, sql, data,
                              template="(%(id)s, %(type)s, %(timestamp)s, %(subject)s, %(email)s, %(response)s)",
                              page_size=10000, fetch=False)
        connection.commit()

    cs_proactive_load = PythonOperator(
        task_id='cs_proactive_load',
        python_callable=cs_proactive_load,
        op_kwargs={'connection_id': 'marketing_conn_id'},
        dag=dag
    )

    end = DummyOperator(
        task_id='end',
        dag=dag
    )

    start_load >> cs_proactive_load >> end
